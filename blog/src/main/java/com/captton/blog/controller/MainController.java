package com.captton.blog.controller;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.captton.blog.model.InputComment;
import com.captton.blog.model.InputPostPost;
import com.captton.blog.model.Post;
import com.captton.blog.model.PostComment;
import com.captton.blog.model.User;
import com.captton.blog.repository.*;

@RestController
@RequestMapping(path = { "/social" })
public class MainController {

	@Autowired
	private DaoUser daouser;

	@Autowired
	private DaoPost daopost;

	@Autowired
	private DaoPostComment daopostcomment;

	@PostMapping(path = { "/altauser" })
	public User altaUsuario(@RequestBody User user) {

		return daouser.save(user);

	}

	@GetMapping(path = { "/getusers" })
	public ResponseEntity<Object> getUsers() {

		List<User> usersFound = daouser.findAll();
		JSONObject obj = new JSONObject();

		if (usersFound.isEmpty()) {

			try {
				obj.put("error", 1);
				obj.put("Message", "No se encontraron usuarios");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return ResponseEntity.ok().body(obj.toString());
		} else {
			JSONArray usersArray = new JSONArray();
			for (User us : usersFound) {
				JSONObject aux = new JSONObject();

				try {
					aux.put("name", us.getName());
					aux.put("fecha de nacimiento", us.getFechanac());
				} catch (JSONException e) {
					e.printStackTrace();
				}
				usersArray.put(aux);

			}
			try {
				obj.put("error", 0);
				obj.put("results", usersArray);
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return ResponseEntity.ok().body(obj.toString());
		}

	}

	@PostMapping(path = { "/post" })
	public ResponseEntity<Object> crearPost(@RequestBody InputPostPost inputPost) {

		User us1 = daouser.findById(inputPost.getIdUser()).orElse(null);

		Post post = new Post();
		JSONObject obj = new JSONObject();
		if (us1 != null) {

			post.setUser(us1);
			post.setComment(inputPost.getPost().getComment());
			post.setTitle(inputPost.getPost().getTitle());
			/**
			 * Hasta que no ejecuto la funcion de dao.save() no existe en la BD el objeto.
			 * Solo dps de que realizo la funcion dao.save() la instancia existe en la BD y
			 * ahi aparece la ID.
			 */
			Post posteo = daopost.save(post);
			try {
				obj.put("error", 0);
				obj.put("results", posteo.getId());
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return ResponseEntity.ok().body(obj.toString());

		} else {
			try {
				obj.put("error", 1);
				obj.put("message", "User not found");
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return ResponseEntity.ok().body(obj.toString());

		}

	}

	@PostMapping(path = { "/commentpost" })
	public ResponseEntity<Object> crearComment(@RequestBody InputComment inputComm) {

		User us1 = daouser.findById(inputComm.getIdUser()).orElse(null);
		Post pos = daopost.findById(inputComm.getIdPost()).orElse(null);

		PostComment compost = new PostComment();
		JSONObject obj = new JSONObject();
		if (us1 != null) {
			if (pos != null) {

				compost.setUser(us1);
				compost.setComment(inputComm.getComment());
				compost.setPost(pos);

				PostComment posteo = daopostcomment.save(compost);
				try {
					obj.put("error", 0);
					obj.put("results", posteo.getId());
				} catch (JSONException e) {
					e.printStackTrace();
				}

				return ResponseEntity.ok().body(obj.toString());
			} else {
				try {
					obj.put("error", 1);
					obj.put("message", "Post not found");
				} catch (JSONException e) {
					e.printStackTrace();
				}

				return ResponseEntity.ok().body(obj.toString());

			}
		} else {
			try {
				obj.put("error", 1);
				obj.put("message", "User not found");
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return ResponseEntity.ok().body(obj.toString());

		}

	}

	@GetMapping(path = { "/getcomments/{id}" })
	public ResponseEntity<Object> getCommentsFromPost(@PathVariable Long id) {
		Post postFound = daopost.findById(id).orElse(null);
		List<PostComment> commentsFound = daopostcomment.findByPost(postFound);
		JSONObject obj = new JSONObject();

		if (commentsFound.isEmpty()) {

			try {
				obj.put("error", 1);
				obj.put("Message", "Este post no tiene comentarios.");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return ResponseEntity.ok().body(obj.toString());
		} else {
			JSONArray commArray = new JSONArray();
			for (PostComment postc : commentsFound) {
				JSONObject aux = new JSONObject();
				// Crear JSON en aux para guardar en el array
				try {
					aux.put("comment",postc.getComment());
					aux.put("post",postc.getPost().getTitle());
					aux.put("user", postc.getUser().getName());
				} catch (JSONException e) {
					e.printStackTrace();
				}
				commArray.put(aux);

			}
			try {
				obj.put("error", 0);
				obj.put("results", commArray);
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return ResponseEntity.ok().body(obj.toString());
		}

	}

}
