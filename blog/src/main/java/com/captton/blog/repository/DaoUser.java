package com.captton.blog.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.captton.blog.model.User;

public interface DaoUser extends JpaRepository<User	, Long> {

}
